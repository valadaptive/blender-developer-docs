# Release on Windows Store

!!! Note
    This procedure has changed to use Buildbot. Needs updating.

work-in-progress playbook for release on Windows Store

## Requirements

- As clean Windows host as possible.
- MSIX packaging tool. Install from [its own Windows Store
  page](https://www.microsoft.com/en-us/p/msix-packaging-tool/9n5lw3jbcxkf)
- Official Blender installer (.msi)
- Microsoft Live login credentials for Blender Foundation to its store
  page
- No pending reboots

### In the future

- Have a virtual machine with a clean Windows install to create the
  package on

## Steps

1. Use the create_msix_package.py script
   ([D8310](https://developer.blender.org/D8310) until this has landed)
2. For an LTS release that is also the current latest release run the
   packaging script **twice**: once with the `--lts`, once without the `--lts`
   switch
3. Login to Windows Store page
4. Update the existing package
   (`Blender` for the latest release, `Blender X.YZ LTS` for the corresponding LTS)

   1. This will create a new submission
   2. Upload the newly created package
   3. Deselect the old version(s), leaving only the new package selected
   4. Submit

      1. Upload new package

      2. Edit the Store Listing - make sure the release notes section version
         number and info gets properly updated

Once the new package has been submitted it takes anywhere between
several hours to several days for the submission to complete, after
which it can take up to 24 hours for the update to be available on all
Windows Store markets.

# What To Do When This Is A New LTS Release

1.  Log in to the Microsoft Store dashboard (access to shared
    credentials needed)
2.  Create a new application
3.  Name it along the form of `Blender X.YZ LTS`, i.e. `Blender 2.83 LTS`
4.  When the first submission is created fill out all necessary
    information
    1.  You can copy&paste most from the `Blender` application.
    2.  Ensure the correct age badges are set, unfortunately one can't
        copy the ID from other applications
    3.  For the listing I suggest exporting the one from `Blender`,
        take out the links for the images, tweak the text, and import it
        (en-US). Don't forget to upload the imagery for the listing
    4.  Ensure that version mentioned throughout this new application is
        correct

# TODOs

- Ensure all icon assets are created, see
  - <https://docs.microsoft.com/en-us/windows/uwp/design/style/app-icons-and-logos#more-about-app-icon-assets>
  - <https://docs.microsoft.com/en-us/windows/uwp/design/style/app-icons-and-logos#icon-types-locations-and-scale-factors>

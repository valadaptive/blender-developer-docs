# Steam Store

Blender is available on the Steam store. Different versions and daily
builds are available under Betas.

## Publishing Builds

Delivery is automated through the buildbot.

Assigning a build as the stable release is done manually on the
[application page](https://partner.steamgames.com/apps/builds/36567).
Before doing so, switch the matching daily build branch and test it
works correctly first.

## Event

!!! Note
    This information may be outdated.

We also create an announcement on the steam platform to deploy the
build - done on <https://steamcommunity.com/app/365670>, then click the
Hub Admin button, Post New Announcement. To be coordinated with the rest
of the release PR. In general you'll get copy from PR, but below is some
template in case you don't.

**Title**: *"Blender 2.83 Official Release is available"*

**Second title**: *<one-line teaser from release notes>*

**Text**:

``` text
<initial paragraph from release notes> Read more about the full feature set on <a href="https://www.blender.org/download/releases/2-82/">blender.org</a>.

<IMAGE SCREENSHOT>

If you want to support Blender development, consider joining the <a href="https://fund.blender.org/">Blender Development Fund</a>.
```

**Images**:

- Screenshot - delete `recent-files.txt`;
  run with `-p 0 0 1920 1080 --factory-startup` and screenshot without the window border,
  to be used in the text where `<IMAGE SCREENSHOT>`.

<!-- -->

- Splash screen - the `splash_2x.png` file converted to `jpg`.

<!-- -->

- Cover Image / Banner - 800x450 for event
- Header Image - 1920x622 for event
- Library Spotlight Banner - 2108x460 for event

### Update Store Graphics

Note: images have maximum allowed file size of 5120Kb.

Under Store Page Admin \> [Graphical Assets](https://partner.steamgames.com/admin/game/edit/34584?activetab=tab_graphicalassets)

#### Store Assets

- From splash screen:
  - 460px x 215px for Header Capsule Image, named `header.jpg`
  - 616px x 353px for Main Capsule Image, name `capsule_616x353.jpg`

#### Library Assets

- Detail from banner with Blender logo overlayed
  - 600px x 900px for Library Capsule Image named
    `library_600x900.jpg`
  - 3840px x 1240px for Library Hero Image named `library_hero.jpg`

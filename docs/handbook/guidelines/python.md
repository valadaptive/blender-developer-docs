# Python Style Guide

Python code should adhere to [PEP 8](https://www.python.org/dev/peps/pep-0008/), with the following
clarifications:

- Only use four spaces for indentation, no tabs.
- Use Unix-style end of line (`LF`, aka `'\n'` character).
- Spaces around operators (except for keyword arguments).
- Use `CamelCase` for classes and exception types. Use `underscore_case`
  for everything else.

## Automated Formatting

Most Python code is automatically formatted using
[autopep8](https://pypi.org/project/autopep8).

Use `make format` to format both C/C++ and Python code in the entire
repository, or integrate it in your IDE.

## Additions to PEP 8

### Naming

- Avoid shadowing names of Python built-in
  [functions](https://docs.python.org/3/library/functions.html),
  [constants](https://docs.python.org/3/library/constants.html),
  [types](https://docs.python.org/3/library/stdtypes.html), and
  [exceptions](https://docs.python.org/3/library/exceptions.html).
  Instead of using `object`, use `obj`; as for other names, the
  Python built-ins are very generic, so often it is better to be more
  specific in your naming. For example, instead of naming a list
  `list`, name it `objects_to_export`. Not only does this avoid
  shadowing a built-in name, it also is more explicit and emphasizes the
  contents of the list. If you want to make the type of the variable
  explicit, use [type annotations](https://docs.python.org/3/library/typing.html).
- Avoid overly short names. Use `mesh` and `curve` instead of `me`
  and `cu` (or worse, `m` and `c`).

### Unused Variables & Arguments

It's sometimes necessary to declare variables or arguments that aren't
used, in this case it's a common convention to begin the variable with
an underscore.

Tools such as pylint will skip them when reporting unused variables.

For example, a menu's draw method always takes a context argument, which
you don't necessarily need.

``` Python
class EXAMPLE_MT_menu(Menu):
    bl_label = ""

    def draw(self, _context):
        layout = self.layout
        layout.menu("EXAMPLE_MT_submenu_a")
        layout.menu("EXAMPLE_MT_submenu_b")
```

Or when iterating over a sequence some items may not be used.

``` Python
for (vert, _edge, face) in elements:
    process(vert, face)
```

## Exceptions to PEP 8

- Line width:  
  Maximum width for lines is 120 for all scripts.
- Imports:  
  We often put imports within functions body which is **not** pep8
  compliant.  
  This is done to speed up Blender's startup times to avoid loading in
  many files and libraries which the user may never access.

## Conventions for Core Scripts

These scripts run as part of Blender at startup (`scripts/startup` and
scripts in `scripts/modules` that run on startup). There are some
additional conventions for these scripts.

- Postpone importing Python modules, where imports may be in a function or a method body.

  This is done for faster startup times and avoid unnecessary overhead
  when running tests or rendering in background mode.

- No type annotations (besides those necessary for `bpy.props` use).

  Currently we don't have a practical way to validate these, see
  [this task](https://developer.blender.org/T87333) for details.

- Use `str.format(..)` to format strings (no f-string or percentage-formatting).

  - Pass positional arguments to `str.format`.

  - Use type specifiers such as `{:s}`, `{:d}`, `{:f}`
    for strings, integers and floats respectively.
    For `repr(..)`: `{!r}` can also be used.

  - In some exceptional cases where it's more difficult to keep track of positional arguments,
    developers may optionally pass keyword arguments to `str.format`.

  See Python's
  [string formatting](https://docs.python.org/3/library/string.html#format-string-syntax)
  documentation for details.

  Note that `str.format` is used in favor of f-strings because string literals often need to be
  translated into other languages. It makes the code-base more straightforward to use one method
  of string formatting.
  (see: [Python String Formatting](https://projects.blender.org/blender/blender/issues/120453)
  for details).

- Single quotes for enumerator literals, double quotes for everything else.
  
  Use single quotes for enums in checks such as `ob.type == 'MESH'`,
  double quotes for other strings such as `layout.label(text="Label Text")`.

## Related Topics

- See: [Blender's Dev Tools repo (includes style checker)](../tooling/blender_tools.md)

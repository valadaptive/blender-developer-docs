# Blender 2.83: Add-ons

# Addons Updates

## Collection Manager

|Collection Manager|QCD Slots|
|-|-|
|![](../../images/Collection_Manager_blender_2.83.png){style="width:429px;"}|![](../../images/Изображение.png){style="width:429px;"}|

- A New Quick Content Display (QCD) system was added in addition to the
  Collection Manager popup. This allows you to set up to 20 collections
  as "slots" and then quickly and easily view them, and move objects to
  them, with GUI widgets or hotkeys.
  (blender/blender-addons@b752de9e)
- Added preferences for QCD.
  (blender/blender-addons@b752de9e)
  (blender/blender-addons@e61a7e8b)
- Added Scene Collection support. This allows you to set it as the
  active collection, move objects to it, and it encompasses the global
  Restriction Toggles which provide actions that affect all collections.
  (blender/blender-addons@dccf56e0)
- Added a Copy/Paste Restriction Toggle states function to all global
  Restriction Toggles.
  (blender/blender-addons@57462fb0)
- Added a Swap Restriction Toggle states function to all global
  Restriction Toggles.
  (blender/blender-addons@f149e0a4)
- Added a Discard History function to all global and local Restriction
  Toggles, and Disclosure (small triangle icon) buttons.
  (blender/blender-addons@61f1c0ae)
- Added an Isolate Expanded Tree function to the Disclosure
  buttons.(blender/blender-addons@1d722cb7)
- Switched from syncing the active collection to the tree view row to
  setting the active collection by clicking on the collection icon.
  (blender/blender-addons@fa71f709)
- Improved the functionality of moving objects with the Collection
  Manager popup allowing objects to be moved to multiple excluded
  collections.
  (blender/blender-addons@5c9aaca6)
- Added a Nested Isolation function to all local Restriction Toggles.
  (blender/blender-addons@05994562)
- Added more protections for the main Collection Manager popup's
  internal states.
  (blender/blender-addons@507c04aa)
  (blender/blender-addons@cc1a2f5a)
- Fixed not being able to move objects while in Local View.
  (blender/blender-addons@5c9aaca6)

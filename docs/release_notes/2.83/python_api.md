# Blender 2.83: Python API

## Compatibility

- Add-ons: in `bl_info`, `wiki_url` was renamed to `doc_url`.
  `wiki_url` is now deprecated.
  (blender/blender@a0ea0153c262)
- Custom shaders need to be patched using the function
  `blender_srgb_to_framebuffer_space`. This ensure the output color is
  transformed to the correct color space.
  (blender/blender@21c658b718b9,
  [\#74139](http://developer.blender.org/T74139))

Example of a patched fragment shader:

``` GLSL
out vec4 out_color;

void main() {
  vec4 srgb_color = vec4(0.5);
  out_color = blender_srgb_to_framebuffer_space(srgb_color);
}
```

- Blender version numbers changed to standard `MAJOR.MINOR.PATCH`
  format. Most scripts will continue to work without changes.
  - `bpy.app.version` now returns `(MAJOR, MINOR, PATCH)`. The
    `PATCH` version is `0` for the first stable release and
    increases for every bugfix release. Sub-version numbers or letters
    are no longer used.
  - `bpy.app.version_string` now includes `Alpha` or `Beta` for
    releases under development, for example `2.90 Alpha`.
  - Add-ons compatible with Blender 2.83 should set `"blender": (2, 83, 0)` in `bl_info`,
    or lower if compatible with earlier versions.

## New

- Nodes: new `bl_icon` on custom nodes to display an icon in the node
  header.
  (blender/blender@120a38c).
- New `foreach_set` and `foreach_get` methods on array properties
  like image pixels. This is significantly more efficient than accessing
  individual elements.
  (blender/blender@9075ec8269e7)

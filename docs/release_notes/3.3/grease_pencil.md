# Grease Pencil

## Line Art

Current performance for LineArt:

![](../../images/LineArt_CAS_improvements.png)

1. New object loading code significantly increased object loading speed
   (approx. 4~8 times faster).
   (blender/blender@03aba8046e07)
   1. Removed "Remove Doubles" option from Geometry Processing panel,
      this is the result of using new object load code path, we do not
      add "remove doubles" as this will take a lot of time to process.
      The user is responsible for ensuring good mesh quality.
2. New multithread quad-tree building method makes triangle insertion
   and intersection calculation much faster
   (blender/blender@432c4c74ebe6).
   Overall performance figure can be seen in the picture above.
   1. There's initially another acceleration method that uses `Atomic Compare and Swap` algorithm
      (blender/blender@14a5a91e0e03),
      however we have since found that algorithm is not preferred in
      terms of OS thread scheduling, so we used the more traditional
      locking method instead.

![](../../images/LineArt_Shadow_Spheres.png){style="width:200px;"}

- New Shadow and Light Contour calculation. Now LineArt is able to
  calculate accurate cast shadow and light/shadow separation line given
  a light source reference object.
  (blender/blender@6dd8ceef2a)

![](../../images/LineArt_Lit_Region.png){style="width:200px;"}

- Filtering feature lines from lit/shaded regions. This images shows the
  example of selecting marked edges from only lit regions.
  (blender/blender@6dd8ceef2a)

![](../../images/LineArt_Shadow.png){style="width:200px;"}
![](../../images/LineArt_Shadow_Enclosed_Shape.png){style="width:200px;"}

- Enclosed Shape option with lit/shaded region selection, able to give
  accurate 2d shape around light/shadow areas. The second image
  demonstrates this effect.
  (blender/blender@6dd8ceef2a)

![](../../images/LineArt_Silhouette.png){style="width:200px;"}

- New Silhouette functionality. Draw silhouette around selected
  collection, or around individual objects in that collection. LineArt
  is also able to identify intersecting and overlapping silhouette
  geometries.
  (blender/blender@6dd8ceef2a)

![](../../images/LineArt_Intersection_Priority.png){style="width:200px;"}

- Intersection priority functionality. Specify different intersection
  priority levels for different objects, then intersection lines would
  be automatically selected with the object who has the higher
  intersection priority. This saves the effort of manually assigning
  intersection flags for a lot of simple cases. In the image, the blue
  sphere has higher intersection priority, and the intersection line is
  selected together with the blue sphere to style with it.
  (blender/blender@6dd8ceef2a)

## Operators

- New Sculpt Auto masking options at 3 levels: Stroke, Layer and
  Material.
  (blender/blender@ab5d52a6db55)

![](../../images/Automasking.png){style="width:800px;"}

## UI

- Added Material selection menu using key `U` in Sculpt mode.
  (blender/blender@ab5d52a6db55)

![](../../images/Change_active_material.png)

- Now `Move to New Layer` allows to enter the name of new layer.
  (blender/blender@129ea355c8b7)

![](../../images/Move_to_layer.png)

## Modifiers and VFX

- New Noise modifier parameter to define when the randomize noise
  pattern change. Now it's possible to change the noise only in
  keyframes.
  (blender/blender@0d9e22d43ca1)

![](../../images/Noise_Modifier.png)

- New Ping Pong mode for Time Offset modifier.
  (blender/blender@82d7234ed97f)

![](../../images/Offest_Modifier.png)

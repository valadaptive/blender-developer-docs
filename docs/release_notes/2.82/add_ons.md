# Blender 2.82: Add-ons

# Addons Updates

## Rigify

- Some generation settings are stored in the metarig now, instead of
  being global to the file.
  (blender/blender-addons@71565f82)
- The `spines.basic_spine` rig now has new optional FK controls.
  (blender/blender-addons@dd723151)
- Spine and limbs now have an option (off by default) to create an
  arbitrarily movable rotation pivot control, similar to BlenRig.
  (blender/blender-addons@358bc43e)
- A new `basic.pivot` rig can be used as a parent of the spine to add
  a custom pivot between root and torso.
  (blender/blender-addons@358bc43e)
- The default rotation pivot of the foot IK control has been switched
  from toe to ankle by default, but can be switched back.
  (blender/blender-addons@c871246f)
- The Human metarig now uses `super_finger` instead of
  `simple_tentacle`; the finger rig has new B-Bone and layer options.
  (blender/blender-addons@b9a821bf)
- The arm rig can optionally generate an extra wrist pivot control for
  the IK hand, most useful for implementing finger IK.
  (blender/blender-addons@db3e15be,
  blender/blender-addons@7b529777)
- New `basic.raw_copy` rig that copies a bone without adding the ORG
  prefix, allowing copying complex ad-hoc rig setups verbatim.
  (blender/blender-addons@a50e87484)
- The `super_copy` and `raw_copy` rigs now have options to patch the
  parent and constraint targets after bones are generated, for advanced
  ad-hoc rigging.
  (blender/blender-addons@feb02092)
- The `super_finger` rig now has optional experimental simple
  fingertip IK support, similar to some other rigs like BlenRig.
  (blender/blender-addons@a41ceb3a)

## Add Mesh Extra Objects

- New Rock Generator addon
- The rock generator addon has been in addons contrib and nightly builds
  for several years now.
- Integrated now into the "Extra Objects" menu, more people can now get
  to appreciate Paul Marshall's work.

## Mesh Tools

- Mesh Tools has integrated the Mesh Relax addon into it's family.

## Viewport Pies

- New Ctrl/U Save Defaults and Preferences pie. Working across all
  editors, save your settings from the 1 menu.
- Returned: Proportional Edit pie. Rewritten by request. Proportional
  Edit pie offers an alternative to the built in Shift/O proportional
  menu.

## BlenderKit

- Advanced search options have been introduced for models and materials
- UI has been reworked, so now the add-on can be found in the top 3d
  view header.
- Asset Bar now opens automatically after search ends, and can also be
  open after blender starts. This is a big speedup in interaction.
- An empty search searches the latest additions in the database.
- Material thumbnail generator has been improved.

![BlenderKit Procedural/Texture based material
search](../../images/Advanced_material_search_-_procedural.gif "BlenderKit Procedural/Texture based material search")\]

# New Addons

## Import Palettes

- This wonderful addition imports Photoshop and Krita palette files.
  From Antionio Vazquez.

## PDT

- Precision Drawing Tools from Alan Odom and Rune Morling.
- This great new addon introduces some CAD style techniques to the
  Blender toolkit.

## Add Node Presets

- This addon by Campbell Barton is promoted from addons contrib in
  nightly builds to main releases.
- In the expanded addons activation box, select a directory that has
  .blend files with node groups.
- You will now have a new Templates menu containing your nodes in the
  Add \> Node menu across the node editors.

## Collection Manager

- Manage Your Collections in the 3d Viewport. Thanks to Ryan Inch

## Sun Position

- Use real world coordinates to adjust your light to match the sun
  position at any time of day!

## Amaranth Toolkit

- Promoted from nightly builds to release with the thanks to Pablo
  Vazquez and CansecoGPC
- this addon provides many small features and functions to extend
  usability.

# Blender 2.90: Add-ons

# Add-ons Updates

## BlenderKit

- Perpendicular snap (in import settings) improves snapping to sloped
  surfaces or ceilings. Useful for trees on slopes/chandeliers under
  curved ceilings and similar.
- A new fast rating operator was added. Accessible under F shortcut in
  asset bar.
- Ui was reorganized to subpanels, with many small useful tweaks.
- Asset menu is available also in the Selected Model panel. Enables
  'search similar' and other commands for objects that are already in
  the scene.
- More info on [the
  webpage](https://www.blenderkit.com/articles/updates-blender-290/) of
  authors

<figure>
<video src="../../../videos/Perpendicular_snap.mp4" title="Snap objects perpendiculary, for trees or other things on sloped surfaces/ceilings" width="600" controls=""></video>
<figcaption>Snap objects perpendiculary, for trees or other things on
sloped surfaces/ceilings</figcaption>
</figure>

## Collection Manager

![Collection Manager popup with RTOs aligned to the right.](../../images/Collection_Manager_Blender_2.90.png){style="width:429px;"}

### New Features

- Added an option to align RTOs to the right.
  (blender/blender-addons@2aa47457)
- Renamed the Filter Restriction Toggles popover to Display Options.
  (blender/blender-addons@2aa47457)
- Added line separators to the tree view to better differentiate rows,
  especially when RTOs are aligned to the right.
  (blender/blender-addons@2aa47457)
- Added a new Specials menu with items to Remove Empty Collections and
  Purge All Collections Without Objects.
  (blender/blender-addons@711efc3e)
- Added a new Apply Phantom Mode button, this will apply the changes
  made to RTOs and quit Phantom Mode.
  (blender/blender-addons@cee17513)
- Added a linear renumbering option and a constrain to branch option to
  the Renumber QCD Slots Operator; all options can now be combined with
  each other.
  (blender/blender-addons@09133c5a)
- Added menu items for the Collection Manager popup and the QCD Move
  widget to the Object-\>Collection menu.
  (blender/blender-addons@52edc5f4)

### Bug Fixes

- Fixed removing collections not preserving the RTOs of their children.
  (blender/blender-addons@969e77ed)
- Fixed an error when removing a collection with a child that is already
  linked to the parent collection.
  (blender/blender-addons@0657e99e)
- Fixed the QCD Move Widget not accounting for the 3D View bounds when
  first called.
  (blender/blender-addons@2c9bc1e6)
- Vastly increased the performance when there are a large number of
  selected objects.
  (blender/blender-addons@adac42a4)

# Blender 3.0: Add-ons

## New add-on key requirements

Blender wants to implement fair and uniform rules for all add-ons
included in official releases. Therefore [new
requirements](../../handbook/addons/index.md#key-requirements-for-add-ons)
have been added. As a result various add-ons aren't bundled with Blender
anymore or have been updated.

- **BlenderKit** isn't distributed with Blender anymore but can still be
  downloaded from the BlenderKit website.
  (blender/blender-addons@13ecbb8fee64)
- **Archipack** isn't distributed with Blender anymore but can still be
  downloaded from the Archipack website
  (blender/blender-addons@d7517a6f2a69)
- **Magic UV**: The online updater was removed.
  (blender/blender-addons@3c5d373fc4da)

## Collection Manager

### New Features

- Added selection of cumulative objects (objects that are in more than
  one collection) to the specials menu in the CM popup.
  (blender/blender-addons@eba3a31f)
- Added support for creating QCD slots by clicking on the unassigned
  slots on the 3D Viewport header widget (the little Xs). Unassigned
  slots now have a tooltip with more information and hotkeys for doing
  things like creating a new slot and moving the selected objects to it.
  (blender/blender-addons@f583ecf0)
- Added the option to create all missing QCD slots from the Quick View
  Toggles menu so you end up with a full 20.
  (blender/blender-addons@f583ecf0)

### Improved Features

- Updated the tooltip for the Collection Manager's move objects button
  with the title "Send Objects to Collection" (was "Set Object
  Collection").
  (blender/blender-addons@69f875e1)
- Changed the icon for the move objects button, when no objects are
  present in the collection, to better suggest moving objects.
  (blender/blender-addons@69f875e1)
- Updated the invert icon in the filter arrow to use the standard double
  arrows icon.
  (blender/blender-addons@69f875e1)
- Changed the filter collections by selected objects icon to be
  consistent with the move objects button's selected objects icon.
  (blender/blender-addons@69f875e1)

### Bug Fixes

- Fixed a bug where attempting to deselect nested collections fails when
  there are no objects present in the collection you clicked on (the
  parent collection).
  (blender/blender-addons@11aebc01)

## Rigify

- The Rigify Legacy Mode and its preference checkbox have been removed.
  (blender/blender-addons@7412d6c3e4)
- A new modular face rig has been implemented. An upgrade operator
  button is provided similar to older legacy rig types; however for now
  the old face is merely deprecated and can still be used.
  ([\#89808](http://developer.blender.org/T89808),
  blender/blender-addons@2acf22b5932,
  blender/blender-addons@985f6d8c304)
- Some Rigify operations have been added to a new menu to make them
  searchable.
  (blender/blender-addons@27fe7f3a4f)
- If the metarig contains a bone called 'root', it is now used as the
  root control to allow changing its rest position or assigning a custom
  widget.
  (blender/blender-addons@992b0592f14)
- Rigify now refreshes all drivers in the file after generation to fix
  drivers that got stuck due to transient errors.
  (blender/blender-addons@e4d9cf84d00)
- Rigify can now execute a custom python script datablock after
  generation to allow arbitrary post-generation customization without
  losing ability to re-generate.
  (blender/blender-addons@ecf30de46c)
- Rigify now uses mirrored linked duplicates for the widgets for the
  left and right side by default; can be disabled with an option.
  (blender/blender-addons@eed6d6cc132)
- The limb IK system has been changed to improve stability of non-pole
  mode and pole toggle precision, which slightly changes non-pole mode
  output.
  (blender/blender-addons@293cc140f5)

## FBX I/O

- Subsurface: Support for 'Boundary Smooth' option of the Blender
  modifier has been added to the FBX importer and exporter
  (blender/blender-addons@9a285d80167f).

## X3D/WRL I/O

- Basic support for material import has been added
  (blender/blender-addons@0573bc7daeb1).

# Blender 4.1: Sequencer

## Performance

The video Sequencer got many performance optimizations across the board.

- Timeline user interface repaints 3x-4x faster for complex timelines.
(blender/blender@df16f4931e)
- Effects: Glow is 6x-10x faster (blender/blender@fc64f48682), Wipe is 6x-20x
faster (blender/blender@06370b5fd6), Gamma Cross is 4x faster
(blender/blender@9cbc96194e), Gaussian Blur is 1.5x faster
(blender/blender@5cac8e2bb4), Solid Color is 2x faster (blender/blender!117058).
- Various parts of image transformation (blender/blender@1e0bf33b00, blender/blender!117125), movie
frame reading and writing (blender/blender@422dd9404f,
blender/blender@4ef5d9f60f), color management (blender/blender@f3ce0645e4) and
audio resampling (blender/blender@986f493812) were sped up.
- Luma Waveform display calculation is 8x-15x faster
(blender/blender@93ca2788ff).

## Filtering

Image/movie filtering that is done when scaling/rotating strips has been improved.

- Default strip filter is now "Auto", which automatically chooses the most appropriate
  filter based on scaling factors (blender/blender!117853):
  - No scale/rotation and integer positions use Nearest,
  - Scaling up by more than 2x uses Cubic Mitchell,
  - Scaling down by more than 2x uses Box,
  - Otherwise Bilinear.
- Strip transforms got Cubic filter option, previously it only
  existed in Transform Effect strip (blender/blender!117100, blender/blender!117517).
  Cubic filter exists in B-Spline (matches cubic elsewhere in Blender) and Mitchell
  (usually better for images) varieties. Cubic filtering is also faster now.
  ![](../../images/VSE41CubicFilters.png)
- Bilinear filter no longer adds a transparent edge near image border when scaling it up.
  (blender/blender!117717)
- Subsampled3x3 filter was replaced by a more general Box filter, that better handles
  scaling images down by more than 3x. (blender/blender!117584)
- Various "off by one pixel" issues resulting in gaps between neighboring strips,
  or images being shifted by half a pixel, have been solved. (blender/blender!116628)

## Scopes

Sequencer Scopes got visual look improvements (blender/blender!116798):

Left is Blender 4.0, right is Blender 4.1:
![Histogram](../../images/VSE41ScopeHistogram.png){style="width:800px;"}
![Waveform (Luma)](../../images/VSE41ScopeWaveform.png){style="width:800px;"}
![Waveform (Parade)](../../images/VSE41ScopeParade.png){style="width:800px;"}
![Vectorscope](../../images/VSE41ScopeVecscope.png){style="width:800px;"}

## Audio

- Audio waveforms are now displayed by default, and
  got a display option to show upper half of the waveform only.
  (blender/blender@a95dd8438d) (blender/blender@1be8b51b11)

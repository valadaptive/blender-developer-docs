# Blender 2.80: Dependency Graph

The core object evaluation and computation system in Blender has been
rewritten. This was done to improve performance, particularly on modern
CPUs with many cores.

Objects, modifiers and constraints are now updated in parallel more
often, which improves performance in scenes with many objects and
complex rigs. Driver evaluation has been updated to support
multithreading in many cases, by avoiding Python for simple expressions.
For some complex scenes running on CPUs with many cores, all these
changes can give an order of magnitude better performance.

It also makes it possible to create more flexible driver and constraint
setups, without creating dependency cycles. We also expect to add new
features in future releases that are only possible with this new system:

- New override system
- Multithreaded export of scenes to renderers
- Animation playback while editing in another window

The new dependency graph uses more memory than before, to support a
clearer separation the original edited data, and evaluated data with
modifiers. In practice for production scenes the increase is often
moderate, as other sources of memory usage dominate.

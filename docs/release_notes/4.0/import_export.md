# Blender 4.0: Import & Export

## USD

- Skeleton and blend shape animation import through UsdSkel. (blender/blender!110912)
- Generic mesh attribute import and export for meshes. (blender/blender!109518)
- Light import and export improvements to follow USD conventions more
  closely. (blender/blender!109795)
- Camera import and export improvements. (blender/blender!112905)
- Export can now be extended with Python scripts through hooks. See the
  `USDHook` [API
  documentation](https://docs.blender.org/api/4.0/bpy.types.USDHook.html)
  for details and example code. (blender/blender!108823)

## 3DS

#### Export Animation

- The 3ds exporter now got the property to export animation keyframes.
  (blender/blender-addons!104613)
- Keyframe export is optional to avoid crashing any importers wich can
  not handle the keyframe section.
  (blender/blender-addons@585a682ef8,
  blender/blender-addons@da9bb97bea)

#### Export Hierarchy

- 3ds objects now got hierarchy chunks to preserve the object tree. (blender/blender-addons!104682)
- The object tree can now be exported without the keyframe section
  needed.
  (blender/blender-addons@0d26a2a017)

#### Export Scale

- Global scale for exporting a 3ds can now be setted. (blender/blender-addons!104767)
- Since many applications are using millimeters, this option is useful
  to convert the measure units.
  (blender/blender-addons@17f142e55d)

#### Convert Measure

- The 3ds importer and exporter now got the ability to converts the unit
  measure for all metric and imperial units. (blender/blender-addons!104769)
- Many 3ds files were exported in millimeter unit scale, this option
  converts too big meshes to Blender meter scale.
  (blender/blender-addons@675987e938)

#### Object Filter

- Both, the importer and the exporter got the new option to filter
  objects for import and export. (blender/blender-addons!104778,
  blender/blender-addons!104782)

#### Volume, fog, gradient and background bitmap

- Atmosphere settings, layerfog parameter and background image can now
  be imported and exported. (blender/blender-addons!104795,
  blender/blender-addons@6fad9e9725)
- Gradient and fog chunks can now be imported and exported. (blender/blender-addons!104811,
  blender/blender-addons@a53788722b,
  blender/blender-addons@7cd6969b3e)

#### Spotlight aspect and projector

- If a spotlight includes a gobo image, it will be exported in the light
  bitmap chunk and imported the same way. (blender/blender-addons!104813,
  blender/blender-addons@f1e443f119)
- The x/y scale of a spotlight can now be exported in the aspect ratio
  chunk, the importer calculates the aspect back to x/y scale. (blender/blender-addons!104815,
  blender/blender-addons@7e2f96796a)

#### Cursor location

- The 3D cursor can now saved to a 3ds file and imported again. (blender/blender-addons!104797,
  blender/blender-addons@75ea7633f6)

#### New Principled BSDF material support

- Moved specular texture to specular tint. (blender/blender-addons!104918,
  blender/blender-addons@fef728a568)
- Added transmission and coat weight parameters.
  (blender/blender-addons@a5d3364c33,
  blender/blender-addons@d82ce1770f,
  blender/blender-addons@9d57b190b8,
  blender/blender-addons@8f6d8d9fc9)
- Added tint colors for coat and sheen.
  (blender/blender-addons@ec069c3b6a)

## FBX

- FBX binary file reading and parsing speed has been improved.
  (blender/blender-addons@890e51e769,
  blender/blender-addons@a600aeb2e2)
- Animation import performance was made faster.
  (blender/blender-addons@abab1c93437322bbf882079a43323e353a5c5b5f,
  blender/blender-addons@e7b19628938d57498b5d9c23bef2bd14610f41e7)
- Animation export performance was made faster.
  (blender/blender-addons@73c65b9a44ff6a5182d433bc991359a7a2a43676,
  blender/blender-addons@461e0c3e1e0a8748106189fde3c7daf61c9fb46b)
- Vertex Group export performance was made faster.
  (blender/blender-addons@63d4898e1d9ebf59abd86e7b4b02ce29b273dc7f)
- Armature data custom properties can now be imported and exported.
  (blender/blender-addons@2b8d9bf2b8f229adb150fdbcfd901fe6fad3c43d).
- Shape keys can now be exported when Triangulate Faces is enabled or
  when Objects have Object-linked materials.
  (blender/blender-addons@bc801d7b1dad4be446435e0cab4d3a80e6bb1d04)

## OBJ and PLY

The OBJ and PLY I/O add-ons have been removed. Importing and exporting
this format is now implemented natively in Blender, with significantly
better performance.
(blender/blender-addons#104504,
blender/blender-addons@67a4aab0da2f3,
blender/blender-addons#104503,
blender/blender-addons@67a4aab0da2f3).

## Collada

- Armatures exported to Collada now include the armature's bone
  collections, including their visibility
  (blender/blender@83306754d48).
  More info about [bone
  collections](animation_rigging.md#bone-collections-and-colors).

## glTF 2.0

Find the changelog in the
[Add-ons](add_ons.md) section.

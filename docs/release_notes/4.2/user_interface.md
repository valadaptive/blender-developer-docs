# Blender 4.2: User Interface

## General

- Support for dropping strings & URL's into the text editor & Python console
  (blender/blender@64843cb12d543f22be636fd69e9512f995c93510)
![Drag Urls and Texts to Text and Console Editors](DragToTextAndConsole.png)  
- Support for horizontal rules in vertical layouts, vertical rules in horizontal.
  (blender/blender@4c50e6992f)
- Improved property dialogs.
  (blender/blender@8fee7c5fc7, blender/blender@da378e4d30, blender/blender@615edb3f3c)
- Changes to menu separator padding. (blender/blender@48dee674f3)
- Fixed pixel shift in Windows clipboard image paste. (blender/blender@a677518cb5)
- Improved square color picker when shown very wide. (blender/blender@857341a7a7)
- Improved large confirmation dialogs.
  (blender/blender@00b4fdce2c, blender/blender@6618755912, blender/blender@3bacd93ba6,
  blender/blender@2c8a3243ee, blender/blender@5eda7c9fd3, blender/blender@311e0270dd,
  blender/blender@d3798970199a6b58efc3701785829d17561ea352)
- Improved small confirmation dialogs. (blender/blender@443ea628c5)
- Outliner "Orphan Data" mode renamed to "Unused Data". (blender/blender@72be662af4)
- Cleanup menu gets new "Manage Unused Data" operation. (blender/blender@f04bd961fd)
- Improved User Interface scaling.
  (blender/blender@85762d99ef, blender/blender@f85989f5db, blender/blender@196cd48aad,
  blender/blender@23aaf88233, blender/blender@1f1fbda3ee)
- Image Copy and Paste now works in Linux/Wayland. (blender/blender@692de6d380)
- Icon changes. (blender/blender@42e7a720c9, blender/blender@4e303751ff)
- Tooltip changes. (blender/blender@3d85765e14, blender/blender@08cd80a284)
- Preferences "Translation" section changed to "Language". (blender/blender@73d76d4360)
- Improved operators for adding/deleting custom themes. (blender/blender@a0a2e7e0dd)
- Improved operators for adding/deleting custom key configurations. (blender/blender@57729aa8bb)
- Reduced dead zone for 3Dconnexion SpaceMouse. (blender/blender@d739d27a2d)
- Optimizations to the font shader. (blender/blender@a05adbef28)
- Panels now supported in dialog popups. (blender/blender@aa03646a74)
- Interface font updated to Inter 4.0. (blender/blender@f235990009)
- New button to save custom themes. (blender/blender@ee38a7b723)
- Corrected initial viewport tilt. (blender/blender@913acaf395)
- Recent Items context menu now allows opening file location. (blender/blender@6789a88107)
- Some operator confirmations removed. (blender/blender@6dd0f6627e)
- Improved confirmation when reverting the current file. (blender/blender@75a9cbed24)
- Tighter Status Bar keymap spacing. (blender/blender@65bfae2258)
- Changes to Splash Screen for new versions. (blender/blender@d2d810f11a)

## Unused IDs Purge UI Improvements

The purge operation now pops-up a dialog where user can choose which options to apply,
and get instant feedback on the amounts of unused data-blocks will be deleted.
blender/blender!117304, blender/blender@0fd8f29e88

![New Purge UI popup](ui_new_purge_popup.png)

## Modifiers
- It's now possible to add, remove, apply, and reorder modifiers all selected objects by holding alt (blender/blender@9a7f4a3b58d9),

## Platform-Specific Changes

- Linux: Registering & unregistering file-type associations is now supported on Linux.
  This can be accessed via the system preferences "Operating System Settings" or via
  command line arguments (`--register`, `--register-allusers`), see `--help` for details.
  (blender/blender@9cb3a17352bd7ed57df82f35a56dcf0af85fbf03).

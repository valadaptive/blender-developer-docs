# Blender 4.2: Add-ons

## 3DS

- Import is now possible by drag & drop from desktop window.
  (blender/blender-addons@79077f3b7f)
- Multiple selected files can now be imported.
  (blender/blender-addons!105227)
- Added option to create new collections for imported files.
  (blender/blender-addons!105232)
- Enabled the collection exporter feature.
  (blender/blender-addons@8250852c04)
- Added support to export non-mesh objects.
  (blender/blender-addons@540de86d70)
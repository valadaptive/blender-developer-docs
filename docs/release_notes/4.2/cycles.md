# Blender 4.2: Cycles

## Shaders

* [Ray Portal BSDF](https://docs.blender.org/manual/en/4.2/render/shader_nodes/shader/ray_portal.html): Transport rays to another location in the scene, with specified ray position and normal. It can be used to render portals for visual effects, and other production rendering tricks. (blender/blender!114386)
* Principled Hair with the Huang model now provides more accurate results when viewing hairs close up, by dynamically switching between a near and far field model depending on the distance. (blender/blender!116094)
* Subsurface Scattering node now has a Roughness input, matching the Principled BSDF. (blender/blender!114499)

## Other

* Improved volume light sampling, particularly for spot lights and area light spread. (blender/blender!119438)
* World Override option for view layers. (blender/blender!117920)
* OpenImageDenoise is now GPU accelerated on AMD GPUs on Windows and Linux.

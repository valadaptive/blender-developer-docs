# Blender 4.2 LTS Release Notes

Blender 4.2 LTS is currently in **Alpha**. Phase **Bcon2** until June 5,
2024.
[See schedule](https://projects.blender.org/blender/blender/milestone/19).

Under development in [`main`](https://projects.blender.org/blender/blender/src/branch/main).

* [Animation & Rigging](animation_rigging/index.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Nodes & Physics](nodes_physics.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [Sequencer](sequencer.md)
* [User Interface](user_interface.md)
* [Add-ons](add_ons.md)

## Compatibility

On Windows and Linux a CPU with SSE4.2 is now required.
This is supported since AMD Bulldozer (2011) and Intel Nehalem (2008).

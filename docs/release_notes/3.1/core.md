## Library Overrides

- Report in the Outliner when a library contains library overrides
  data-blocks that required some resyncing
  ([D13766](http://developer.blender.org/D13766),
  blender/blender@a909ab984ce4).

## Batch Rename

- Support for renaming selected items in the outliner
  (blender/blender@75d84b1b642a82777d2688a9bdeedd7064dcee10).
- Support for renaming collections, volumes and lights
  blender/blender@54fb1a75ee191e5d97cda29a1e4dc78b4563bfb3,
  blender/blender@c11c2a4b91d57bed588615734da858ac3c2af5b3,
  blender/blender@7220897280e285305d8ab908a3ca95f8311dfa3e).

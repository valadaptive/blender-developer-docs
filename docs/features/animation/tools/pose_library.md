# Animation Tools: Pose Library

Blender currently has a pose library system, where an Action can be
marked as Pose Library. Poses can be marked with Pose Markers in that
action, or via the Pose Library panel in the armature properties panel.

A new pose library system is currently in its [design
phase](https://developer.blender.org/project/profile/126/).

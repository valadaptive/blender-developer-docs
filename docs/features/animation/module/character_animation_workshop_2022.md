# Character Animation workshop (2022)

The goal of this three-day workshop will be to come up with a design for
a new character animation system in Blender. It'll be the kickoff of a
3-year project *Animation 2025*.

## Presentation and Results

The presentation is available on YouTube: [The Future of Character
Animation & Rigging: a Q&A feedback
session](https://www.youtube.com/watch?v=msOjFC03JRY)

- [Design Principles](https://docs.google.com/document/d/1qjD1MhXAFnnySSzey9DW-vumhtNavUxqCbBM9P30FzQ/edit)
- [What's stopping you from animating in Blender?](https://docs.google.com/document/d/11_7rUJ-UJXH9vEMHHbq4oRAOkm71hjR9INcrKNWpB9E/edit)
- [Blank Slate - Character Animation in Blender](https://docs.google.com/document/d/1fvxezZJHHHsa7bcufdRFpWyrqjQuVHqxNTUc5Sg0VCI/edit)
- [PDF of the Mural workboard used during the workshop](https://download.blender.org/institute/sybren/animation-rigging/2022-character-animation-workshop/2022-animation-workshop-mural-download.pdf)
  (12½ MB)

<figure>
<img src="../../../images/2022-animation-workshop-big-blocks.webp"
title="Bigger topics for planning|thumb|center" />
<figcaption>Bigger topics for planning|thumb|center</figcaption>
</figure>

## Dates & Locations

**Date:** 24-26 October 2022

**Location:** [Blender Institute](https://www.blender.org/about/institute/)

**Remote:** The main communication channel is [\#animation-module on Blender Chat](https://blender.chat/channel/animation-module). Video
meeting will happen via [Google Meet on the usual meeting ID](https://meet.google.com/ora-nbxv-evf).

## Show & Tell

If you're joining the workshop, or even if you just want the
workshop-peoples to see something you think is important (max 5 minute
video), **please use [this sign-up
sheet](https://docs.google.com/forms/d/e/1FAIpQLSey84FhfB6yx-bf_GMqIgiIwhy05W-ystMfb_LVLLMA-U_HuA/viewform)**.
It'll help us streamline the process.

## Schedule

Every day, there will be three "sync moments" to make sure online/remote
attendees are in sync with what's happening at Blender HQ, and vice
versa.

All times are in local time, so CEST / Amsterdam time.

- 10:00 morning kickoff
- 14:00 after lunch
- 18:00 end of day

## Documents

- [Design Principles](https://docs.google.com/document/d/1qjD1MhXAFnnySSzey9DW-vumhtNavUxqCbBM9P30FzQ/edit)
- [What's stopping you from animating in Blender?](https://docs.google.com/document/d/11_7rUJ-UJXH9vEMHHbq4oRAOkm71hjR9INcrKNWpB9E/edit)
- [Blank Slate - Character Animation in Blender](https://docs.google.com/document/d/1fvxezZJHHHsa7bcufdRFpWyrqjQuVHqxNTUc5Sg0VCI/edit)
- [Workshop schedule](https://docs.google.com/spreadsheets/d/1dBNl54Oz4X4S21z83jBFWlAOWTUQ8KPUAmZcxBhkBf4/edit)
- [Sign-up Sheet](https://docs.google.com/forms/d/e/1FAIpQLSey84FhfB6yx-bf_GMqIgiIwhy05W-ystMfb_LVLLMA-U_HuA/viewform)
  ([edit](https://docs.google.com/forms/d/1xVXAszg1wfFj4bcKYxIYkca5rc7pe88Mf7RpNSI_-r0/edit?usp=drive_web))
- [Mural, which served as a work/brainstorm board during the workshop](https://app.mural.co/invitation/mural/home4643/1665674871684?sender=u6f41ad2ad961ab9f6e6a1021&key=91496bec-547f-44bc-a122-3ba0e5d32613)

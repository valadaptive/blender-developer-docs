# Extension Listing API

The specification for all the Extension Listing API versions. Note that Blender should be able to keep supporting previous API versions.

| Version           | Blender Version Initial                      | Blender Version Final                        |
| ----------------- | -------------------------------------------- | -------------------------------------------- |
| [v1](v1.md) | [4.2.0](../../../release_notes/4.2/index.md) | - |
# LookdevView

The lookdev view is used to generate an ortographical view from the viewport
to render the Lookdev HDRI Spheres. The spheres are rendered on the near
clip plane of the view. This ensures that the spheres will always be in view
and should not consider the actual size of the scene.

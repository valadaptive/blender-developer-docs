# Swap Chain

The owner ship of the swap chain is in GHOST_ContextVK. As swap chain is controlled
outside the GPU module (window manager calls directly `GHOST_SwapWindowBuffers` the
actual state between swap chain images can be lost.

To solve this `GHOST_ContextVK` has two callback functions (`swap_buffers_pre_callback`
and `swap_buffers_post_callback`). The pre callback is responsible to fill the given
swap buffer image with the pixels that will be presented to the user. The post callback
can check if the swap chain was recreated and need to update its internal state.

```mermaid
sequenceDiagram
    box editor
    participant WindowManager
    end

    box GHOST
    participant GHOST_Window
    participant GHOST_ContextVK
    end

    box gpu
    participant VKContext
    end

    box vulkan
    participant Vulkan
    end

    WindowManager->>GHOST_Window: swapBuffers
    activate GHOST_Window
        GHOST_Window->>GHOST_ContextVK:swapBuffers
        activate GHOST_ContextVK
            GHOST_ContextVK->>Vulkan: vkAcquireNextImage
            activate Vulkan
            deactivate Vulkan

            GHOST_ContextVK-->VKContext: swap_buffers_pre_callback
            activate VKContext
                note over VKContext: Copy internal framebuffer to swap chain image
                VKContext->>Vulkan: vkCmdCopyImage
                activate Vulkan
                deactivate Vulkan
            deactivate VKContext

            GHOST_ContextVK->>Vulkan: vkQueuePresent
            activate Vulkan
            deactivate Vulkan

            GHOST_ContextVK-->VKContext: swap_buffers_post_callback
            activate VKContext
                note over VKContext: Retrieve swap chain format and update internal framebuffer

            deactivate VKContext

        deactivate GHOST_ContextVK

    deactivate GHOST_Window
```

The benefits of this is that images are only retrieved from the swap chain when they are needed.
giving more time to perform swap chain actions by Vulkan. This approach is similar to the Metal
backend.

## Image orientation

Blender coordinate system is based on OpenGL (top left is origin). In Vulkans coordinate system
the origin is bottom left. This is solved by drawing everything upside down and flipping the
orientation when copying the final framebuffer to the swap chain image.

## References

* `source/blender/gpu/vulkan/vk_context.hh`
* `source/blender/gpu.vulkan/vk_context.cc`
* `intern/ghost/intern/GHOST_ContextVK.hh`
* `intern/ghost/intern/GHOST_ContextVK.cc`

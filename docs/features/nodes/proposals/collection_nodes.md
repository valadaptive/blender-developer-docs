# Collection Nodes

Collection nodes (or modifiers) allow for object operations.

This complements the geometry nodes functionality by letting
users control data in the object level. For instance, to populate a
collection dynamically and set different actions for different objects
inside of it.

## Use Cases

1. Crowd cheering inside a soccer stadium.
2. A tornado taking a house apart, piece by piece ([reference](https://twitter.com/ZanQdo/status/1358533918006378498)).

![](../../../images/EverythingNodes-CollectionModifier-Tornado.png)

## Unit

While geometry nodes operate with geometry, collection nodes operate with objects.

## Nodes

- Separate/Join objects
- Add/remove objects
- Set action
- Set material
- Objects constraints
- Set colliders (?)

## Tornado Example

Node tree for the tornado use case. In this case the tornado is a node
group with its complexity abstracted away.

![](../../../images/EverythingNodes-CollectionNodes-Tornado-Nodes.png)

## Stadium Example

Node tree for the stadium cheering crowd. The points object is using
geometry nodes. But no geometry can be passed in this nodetree.

![](../../../images/EverythingNodes-CollectionNodes-Cheering-stadium.png)
